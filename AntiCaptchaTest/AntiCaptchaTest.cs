﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nuget_AntiCaptcha.Captchas;
using Nuget_AntiCaptcha.Responses;
using System.Threading;

namespace AntiCaptchaTest
{
    [TestClass]
    public class AntiCaptchaTest : TestBase
    {
        private TaskResponse RecaptchaObj = null;

        [TestMethod]
        public void GetBalanceTest()
        {
            BalanceResponse response = null;

            response = Client.GetBalance().Result;
            
            Assert.IsNotNull(response);
            Assert.AreEqual(response.errorId, 0);
        }


        [TestMethod]
        public void ReCaptcha_SubmitTest()
        {
            NoCaptchaTaskProxyless task = new NoCaptchaTaskProxyless
            {
                websiteKey = "6LeYWFsUAAAAAI74vqAyoCMFhOPynvk_J-KxOi4c",
                websiteURL = "https://r.dstoken.io"
            };

            TaskResponse response = Client.SubmitTask(task).Result;

            Assert.IsNotNull(response);
            Assert.AreEqual(response.errorId, 0);
            RecaptchaObj = response;
        }

        [TestMethod]
        public void ReCaptcha_GetResultTest()
        {
            while(RecaptchaObj == null) { Thread.Sleep(1000); }


            NoCaptchaSolution response = Client.GetNoCaptchaSolution(RecaptchaObj.taskId).Result;
            
            Assert.IsNotNull(response);
            Assert.AreEqual(response.errorId, 0);
        }
    }
}
