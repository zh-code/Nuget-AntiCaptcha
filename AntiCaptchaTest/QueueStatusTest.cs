﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nuget_AntiCaptcha.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace AntiCaptchaTest
{
    [TestClass]
    public class QueueStatusTest : TestBase
    {
        [TestMethod]
        public void GetImageToTextEnglishQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.ImageToText_English).Result;

            Assert.IsNotNull(status);
        }


        [TestMethod]
        public void GetImageToTextRussianQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.ImageToText_Russian).Result;

            Assert.IsNotNull(status);
        }


        [TestMethod]
        public void GetNoCaptchaQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.ReCaptchaNoCaptcha).Result;

            Assert.IsNotNull(status);
        }


        [TestMethod]
        public void GetNoCaptchaProxylessQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.RecaptchaNoCaptchaProxyless).Result;

            Assert.IsNotNull(status);
        }


        [TestMethod]
        public void GetFunCaptchaQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.FunCaptcha).Result;

            Assert.IsNotNull(status);
        }


        [TestMethod]
        public void GetFunCaptchaProxylessQueueStatusTest()
        {
            QueueStatusResponse status = null;

            status = Client.GetQueueStatus(Nuget_AntiCaptcha.Enums.CaptchaType.FunCaptchaProxyless).Result;

            Assert.IsNotNull(status);
        }
    }
}
