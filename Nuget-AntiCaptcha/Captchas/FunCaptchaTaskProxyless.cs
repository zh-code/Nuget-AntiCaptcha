﻿namespace Nuget_AntiCaptcha.Captchas
{
    public class FunCaptchaTaskProxyless : ITask
    {
        public string type = "FunCaptchaTaskProxyless";
        public string websiteURL { get; set; }
        public string websitePublicKey { get; set; }

    }
}
