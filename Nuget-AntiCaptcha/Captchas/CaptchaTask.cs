﻿namespace Nuget_AntiCaptcha.Captchas
{
    public sealed class CaptchaTask
    {
        public string clientKey { get; set; }
        public ITask task { get; set; }
        public int softId { get; set; }
    }
}
