﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nuget_AntiCaptcha.Captchas
{
    public class GeeTestTaskProxyless : ITask
    {
        public string type = "GeeTestTaskProxyless";
        public string websiteURL { get; set; }
        public string gt { get; set; }
        public string challenge { get; set; }
    }
}
