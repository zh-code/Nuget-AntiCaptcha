﻿namespace Nuget_AntiCaptcha.Captchas
{
    public sealed class GeeTestTask : GeeTestTaskProxyless
    {
        public new string type = "GeeTestTask";
        public string proxyType { get; set; }
        public string proxyAddress { get; set; }
        public int proxyPort { get; set; }
        public string proxyLogin { get; set; }
        public string proxyPassword { get; set; }
        public string userAgent { get; set; }
    }
}
