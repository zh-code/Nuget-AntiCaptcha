﻿namespace Nuget_AntiCaptcha.Captchas
{
    public sealed class SquareNetTextTask : ITask
    {
        public string type = "SquareNetTask";
        public string body { get; set; }
        public string objectName { get; set; }
        public int rowsCount { get; set; }
        public int columnsCount { get; set; }
    }
}
