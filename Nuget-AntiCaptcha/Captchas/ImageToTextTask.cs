﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nuget_AntiCaptcha.Captchas
{
    public sealed class ImageToTextTask : ITask
    {
        public string yype = "ImageToTextTask";
        public string body { get; set; }
        public bool phase { get; set; }
        public bool @case { get; set; }
        public bool numeric { get; set; }
        public int math { get; set; }
        public int minLength { get; set; }
        public int maxLength { get; set; }
    }
}
