﻿namespace Nuget_AntiCaptcha.Captchas
{
    public class NoCaptchaTaskProxyless : ITask
    {
        public string type = "NoCaptchaTaskProxyless";
        public string websiteURL { get; set; }
        public string websiteKey { get; set; }
    }
}
