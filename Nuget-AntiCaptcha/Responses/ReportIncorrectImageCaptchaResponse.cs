﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class ReportIncorrectImageCaptchaResponse
    {
        public string clientKey { get; set; }
        public int taskId { get; set; }
    }
}
