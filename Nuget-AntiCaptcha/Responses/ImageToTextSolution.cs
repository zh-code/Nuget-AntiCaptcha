﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class ImageToTextSolution: TaskSolution
    {
        public ImageToTextSolutionToken solution { get; set; }
    }

    public sealed class ImageToTextSolutionToken
    {
        public string text { get; set; }
        public string url { get; set; }
    }
}
