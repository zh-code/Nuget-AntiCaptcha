﻿namespace Nuget_AntiCaptcha.Responses
{
    public class TaskSolution
    {
        public int errorId { get; set; }
        public string status { get; set; }
        public decimal cost { get; set; }
        public string ip { get; set; }
        public long createTime { get; set; }
        public long endTime { get; set; }
        public int solveCount { get; set; }
    }
}
