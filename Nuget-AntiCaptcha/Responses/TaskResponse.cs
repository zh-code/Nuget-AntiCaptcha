﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class TaskResponse
    {
        public int errorId { get; set; }
        public long taskId { get; set; }
    }
}
