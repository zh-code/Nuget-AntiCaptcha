﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class NoCaptchaSolution : TaskSolution
    {
        public NoCaptchaSolutionToken solution { get; set; }
    }


    public sealed class NoCaptchaSolutionToken
    {
        public string gRecaptchaResponse { get; set; }
    }
}
