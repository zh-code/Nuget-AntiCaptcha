﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class FunCaptchaSolution : TaskSolution
    {
        public FunCaptchaSolutionToken solution { get; set; }
    }

    public sealed class FunCaptchaSolutionToken
    {
        public string token { get; set; }
    }
}
