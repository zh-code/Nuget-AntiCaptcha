﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class GeeTestSolution : TaskSolution
    {
        public GeeTestSolutionToken solution { get; set; }
    }

    public sealed class GeeTestSolutionToken
    {
        public string challenge { get; set; }
        public string validate { get; set; }
        public string seccode { get; set; }
    }
}
