﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class QueueStatusResponse
    {
        public int waiting { get; set; }
        public decimal load { get; set; }
        public decimal bid { get; set; }
        public decimal speed { get; set; }
        public int total { get; set; }
    }
}
