﻿using System.Collections.Generic;

namespace Nuget_AntiCaptcha.Responses
{
    public sealed class SquareNetTextSolution
    {
        public List<int> cellNumbers { get; set; }
    }
}
