﻿namespace Nuget_AntiCaptcha.Responses
{
    public sealed class BalanceResponse
    {
        public int errorId { get; set; }
        public decimal balance { get; set; }
    }
}
