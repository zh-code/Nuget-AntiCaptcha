﻿using Newtonsoft.Json;
using Nuget_AntiCaptcha.Captchas;
using Nuget_AntiCaptcha.Enums;
using Nuget_AntiCaptcha.Responses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Nuget_AntiCaptcha
{
    public class AntiCaptcha
    {
        private string Api { get; set; } // AntiCaptcha API
        private int AppId = 874; // AntiCaptcha Application ID

        private static HttpClient Client = new HttpClient { BaseAddress = new Uri("https://api.anti-captcha.com/") };

        /// <summary>
        /// Initiate without API key
        /// </summary>
        public AntiCaptcha()
        {
            this.Api = "";
        }

        /// <summary>
        /// Initiate with API key
        /// </summary>
        /// <param name="api">AntiCaptcha API Key</param>
        public AntiCaptcha(string api)
        {
            this.Api = api;
        }

        /// <summary>
        /// Change Application Id
        /// </summary>
        /// <param name="Id">App Id</param>
        public void SetApplication(int Id)
        {
            this.AppId = Id;
        }

        /// <summary>
        /// Get API balance
        /// </summary>
        /// <returns></returns>
        public async Task<BalanceResponse> GetBalance()
        {
            object obj = new { clientKey = Api };
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getBalance", new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        BalanceResponse res = JsonConvert.DeserializeObject<BalanceResponse>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Get queue load statistics
        /// </summary>
        /// <param name="QueueType"></param>
        /// <returns></returns>
        public async Task<QueueStatusResponse> GetQueueStatus(CaptchaType QueueType)
        {
            object obj = new { queueId = QueueType };
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getQueueStats", new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        QueueStatusResponse res = JsonConvert.DeserializeObject<QueueStatusResponse>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;

        }

        /// <summary>
        /// Submit captcha tasks
        /// </summary>
        /// <param name="task">ITask</param>
        /// <param name="languagePool">Default "en"</param>
        /// <returns></returns>
        public async Task<TaskResponse> SubmitTask(ITask task)
        {
            CaptchaTask obj = new CaptchaTask
            {
                clientKey = this.Api,
                task = task,
                softId = this.AppId
            };
            try
            {
                Debug.WriteLine(JsonConvert.SerializeObject(obj));
                using (HttpResponseMessage response = await Client.PostAsync("createTask", new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        TaskResponse res = JsonConvert.DeserializeObject<TaskResponse>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Report incorrect image captcha solvings
        /// </summary>
        /// <param name="Id">Captcha task id</param>
        /// <returns></returns>
        public async Task<ReportIncorrectImageCaptchaResponse> ReportIncorrectImageCaptcha(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("reportIncorrectImageCaptcha", 
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        ReportIncorrectImageCaptchaResponse res = JsonConvert.DeserializeObject<ReportIncorrectImageCaptchaResponse>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Get FunCaptcha Solution
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns></returns>
        public async Task<FunCaptchaSolution> GetFunCaptchaSolution(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getTaskResult ", 
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        FunCaptchaSolution res = JsonConvert.DeserializeObject<FunCaptchaSolution>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Get Gee Test Solution
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns></returns>
        public async Task<GeeTestSolution> GetGeeTestSolution(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getTaskResult ",
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        GeeTestSolution res = JsonConvert.DeserializeObject<GeeTestSolution>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Get Image To Text Solution
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns></returns>
        public async Task<ImageToTextSolution> GetImageToTextSolution(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getTaskResult ",
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        ImageToTextSolution res = JsonConvert.DeserializeObject<ImageToTextSolution>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }


        /// <summary>
        /// Get NoCaptcha Solution
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns></returns>
        public async Task<NoCaptchaSolution> GetNoCaptchaSolution(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getTaskResult ",
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        NoCaptchaSolution res = JsonConvert.DeserializeObject<NoCaptchaSolution>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Get Square Net Text Solution
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns></returns>
        public async Task<SquareNetTextSolution> GetSquareNetTextSolution(long Id)
        {
            try
            {
                using (HttpResponseMessage response = await Client.PostAsync("getTaskResult ",
                    new StringContent(GetResultRequestString(Id), Encoding.UTF8, "application/json")))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        SquareNetTextSolution res = JsonConvert.DeserializeObject<SquareNetTextSolution>(await response.Content.ReadAsStringAsync());
                        return res;
                    }
                }
            }
            catch { }
            return null;
        }

        private string GetResultRequestString(long Id)
        {
            return JsonConvert.SerializeObject(new { clientKey = this.Api, taskId = Id });
        }
    }
}
