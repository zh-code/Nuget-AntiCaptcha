﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nuget_AntiCaptcha.Enums
{
    public enum CaptchaType
    {
        ImageToText_English = 1,
        ImageToText_Russian = 2,
        ReCaptchaNoCaptcha = 5,
        RecaptchaNoCaptchaProxyless = 6,
        FunCaptcha = 7,
        FunCaptchaProxyless = 10
    }
}
