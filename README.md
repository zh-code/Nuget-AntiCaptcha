# AntiCaptcha Nuget Package

This package is open source and free to use.

# Get Started

## 1. Create AntiCaptcha account (if you are new)
[Click to create new account(Referral)](http://getcaptchasolution.com/0q1y1qu6xu)  
After you created your account, go to _Settings -> API Setup_ and copy your API key.
  
## 2. Add balance if you need

## 3. Initiate class
```c#
AntiCaptcha Client = new AntiCaptcha("API Key Goes Here");
```

## 4. Create solving task
There are many captcha tasks available now:
1. FunCaptchaTask
2. FunCaptchaTaskProxyless
3. GeeTestTask
4. GeeTestTaskProxyless
5. ImageToTextTask
6. NoCaptchaTask
7. NoCaptchaTaskProxyless
8. SquareNetTextTask

For tasks that have _proxyless_ at the end, it means no proxy is needed to solve the task.  

Use NoCaptchaTask and NoCaptchaTask as an example  

#### NoCaptcha
```c#
NoCaptchaTask task = new NoCaptchaTask
{
    websiteKey = "6LeYWFsUAAAAAI74vqAyoCMFhOPynvk_J-KxOi4c",
    websiteURL = "https://r.dstoken.io",
    proxyType":"http"
    "proxyAddress":"8.8.8.8"
    "proxyPort":8080
    "proxyLogin":"Proxy Username"
    "proxyPassword":"Proxy Password"
    "userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"
}
```

#### NoCaptchaProxyless
```c#
NoCaptchaTaskProxyless task = new NoCaptchaTaskProxyless
{
    websiteKey = "6LeYWFsUAAAAAI74vqAyoCMFhOPynvk_J-KxOi4c",
    websiteURL = "https://r.dstoken.io"
}
```

## 5. Submit task
```c#
TaskResponse response = await Client.SubmitTask(task);
```

If `response.errorId` is not 0, it means something is wrong and please check [Error Documentation](https://anticaptcha.atlassian.net/wiki/spaces/API/pages/5079075/Errors)

## 6. Get task solution
For different task type please use different method, different task will have different solution structure
1. GetFunCaptchaSolution
2. GetGeeTestSolution
3. GetImageToTextSolution
4. GetNoCaptchaSolution
5. GetSquareNetTextSolution

```c#
NoCaptchaSolution solution = await Client.GetNoCaptchaSolution(response.taskId);
```

If `solution.errorId` is not 0, it means something is wrong and please check [Error Documentation](https://anticaptcha.atlassian.net/wiki/spaces/API/pages/5079075/Errors)


## Misc methods

1. Client.GetBalance()  
    Get API Balance
2. Client.GetQueueStatus(CaptchaType QueueType)
  CaptchaType is Enum:  
    `ImageToText_English`   
    `ImageToText_Russian`  
    `ReCaptchaNoCaptcha`  
    `RecaptchaNoCaptchaProxyless`  
    `FunCaptcha`  
    `FunCaptchaProxyless`  